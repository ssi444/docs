---
id: get-started
title: Getting started with Database Lab
hide_title: false
sidebar_label: Getting started
slug: /
description: Database Lab is used to boost software development via enabling ultra-fast provisioning of databases of any size. Developers, DBAs, and QA engineers work with full-sized independent clones of PostgreSQL databases. Development and testing tasks are accomplished much faster, with more iterations done, with better quality achieved and with much less money spent.
keywords:
  - "postgres.ai clones"
  - "postgresql clones"
  - "Database Lab Engine"
  - "Dev/QA/Staging databases with superpowers"
  - "postgres-checkup"
  - "Joe bot for SQL optimization"
  - "SQL optinization on clones"
  - "thin clones for Postgres"
  - "database testing in CI/CD"
  - "postgres-checkup"
---

|   |   |
| - | - |
| [Database Lab Engine](/docs/database-lab) (open source)<br/>Open-source technology to clone databases of any size in seconds | [Joe, SQL optimization chatbot](/docs/joe-bot) (open source)<br/>Run `EXPLAIN ANALYZE` and optimize SQL on instantly provisioned full-size database copies |
| [Dev/QA/Staging databases with superpowers](/docs/staging)<br/>Develop and test using full-size database copies provided in seconds | [CI/CD observer for DB schema changes](/docs/database-changes-cicd)<br/>Prevent performance degradation and downtime when deploying database schema changes | 
| [postgres-checkup](/docs/checkup) (open source)<br/>Automated health-checks and query analysis for heavily-loaded PostgreSQL databases | [Detached replicas](/docs/data-access)<br/>Use BI tools, run analytical queries, perform data export without replication lags and bloat |
| [Database Lab tutorial for Amazon RDS](/docs/tutorials/database-lab-tutorial-amazon-rds)<br/>Get started to use Database Lab for Amazon RDS PostgreSQL | [Database Lab tutorial](/docs/tutorials/database-lab-tutorial)<br/>Get started to use Database Lab for any PostgreSQL |

<!--#### [Data recovery /  Instantaneously recover lost data](/docs/data-recovery)
Recover accidentally deleted data. Using thin cloning, the point-in-time recovery (PITR) can be performed without long waiting.
-->

## What is Database Lab?
Database Lab is used to boost software development via enabling ultra-fast provisioning of databases of any size. Developers, DBAs, and QA engineers work with full-sized independent clones of PostgreSQL databases. Development and testing tasks are accomplished much faster, with more iterations done, with better quality achieved and with much less money spent.

![CI/CD transformation with Database Lab](/assets/cicd-transform.png)

- Optimize non-production infrastructure costs by 10x.
- Drastically improve development quality.
- Get rid of downtimes and avoid performance degradation.
- Cut half of the TTM (time to market), develop 2x faster than competitors.

![Database Lab architecture](/assets/architecture.png)

:::note Database Lab "Private Beta" program&nbsp;&nbsp;👋

Database Lab Platform (SaaS) is currently in a "private beta" mode, being tested by several hundred engineers. Want to become an early adopter? Join Database Lab by Postgres.ai "Private Beta" program today: https://postgres.ai/console/.

:::
