---
title: Database Lab pricing
sidebar_label: Pricing
description: Compare plans for Database Lab Platform by Postgres.ai.
keywords:
  - "Database Lab features"
  - "postgres.ai features"
  - "postgres.ai database lab pricing"
  - "database lab plans"
---

<!--
-->

[👀 Try Database Lab now for free](https://postgres.ai/console)

## Plans

| Features | Community<br/>Edition | Enterprise<br/>Standard | Enterprise<br/>Premium |
| :------- | :------------------: | :-------------------: | :-------------------: |
| **Features** | **Community<br/>Edition** | **Enterprise<br/>Standard** | **Enterprise<br/>Premium** |
|Price|Free|Pay-as-you-go: <br/>$2.6624 per TiB per hour<br/><br/>Prepaid: starting<br/>at $1,950/month| <a href="mailto:sales@postgres.ai">Contact us</a> |
|||||
|License|AGPLv3|Commercial|Commercial|
|**Thin clones (Database Lab)**|✅|✅|✅|
|**Platform - GUI**|❌|✅|✅|
|**Advanced security features. Personal tokens and access control**|❌|✅|✅|
|**CI integrations**|❌|✅|✅|
|||||
|**SQL optimization environment (Joe Bot)**||||
|Basic SQL optimization|✅|✅|✅|
|Private chats|❌|✅|✅|
|SQL optimization knowledge base|❌|✅|✅|
|||||
|**Support and deployment**||||
|Community support|✅|✅|✅|
|Standard support|❌|✅|✅|
|Premium support (24/7, 1 hour), trainings|❌|❌|✅|

[Discover all features](/docs/all-features)

## Pricing and billing details

- The billing is based on the overall size of your databases (physical size on disk, observed by `df`)
- For billing purposes, Database Lab Engine automatically tracks the size of the data directory on an hourly basis
- Two payment models are available:
    - "Pay-as-you-go" model:
        - $2.6624 / per TiB per hour (with GiB precision),
        - once the billing cycle (a month) is finalized, the Platform automatically generates both the detailed report and the invoice,
        - payments are made based on this invoice and credit card on file ([Stripe](https://stripe.com) is used for all automated payments),
        - this model is most convenient for smaller companies, it is based on usage (calculated hourly).
    - "Enterprise" model:
        - the price is based on the analysis of your company's needs, several Tiers are available:
            - up to 1 TiB: $1,950 monthly (minimum for this model),
            - up to 3 TiB: $5,432 monthly (discount 7.14%),
            - up to 5 TiB: $8,287 monthly (discount 15%),
            - up to 10 TiB: $14,625 monthly (discount 25%),
            - more than 10 TiB: reach out to the Sales team for a special offering;
        - the limit may be exceeded any time without service interruption, but the detailed usage is recorded and reported at the end of the billing period,  
        - billing period options: monthly, quarterly, or annually, 
        - payment is to be made prior to the next billing period, based on an individual contract,
        - reach out to the Sales team to learn more: sales@postgres.ai.
- Two-week trial that could be extended by participating in the Liable Beta Tester program
- Database Lab Engine is always hosted on your infrastructure, and the data never leaves your infrastructure


:::note Database Lab "Private Beta" program&nbsp;&nbsp;👋

Database Lab Platform (SaaS) is currently in a "private beta" mode, being tested by several hundred engineers. Want to become an early adopter? Join Database Lab by Postgres.ai "Private Beta" program today: https://postgres.ai/console/.

:::
